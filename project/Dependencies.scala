import sbt._

object Dependencies {

  // Unit and integration tests
  val scalatest = "org.scalatest" %% "scalatest" % "3.2.3"

  // Property based testing
  val scalacheck = "org.scalacheck" %% "scalacheck" % "1.15.2"

  // Use scalacheck via scalatest
  // The package name MUST correspond to the major.minor version of the scalacheck dependency
  val `scalatest-plus-scalacheck` = "org.scalatestplus" %% "scalacheck-1-15" % "3.2.3.0"

  // Use NOP logger for slf4j to disable logging during testing
  val `slf4j-nop` = "org.slf4j" % "slf4j-nop" % "1.7.30"

  // Circe JSON library
  val `circe-core`    = "io.circe" %% "circe-core"    % "0.13.0"
  val `circe-generic` = "io.circe" %% "circe-generic" % "0.13.0"
  val `circe-parser`  = "io.circe" %% "circe-parser"  % "0.13.0"
  val `circe-yaml`    = "io.circe" %% "circe-yaml"    % "0.13.0"

  // Commandline option parsing
  val scallop = "org.rogach" %% "scallop" % "4.0.1"

  // Mathezirkel database
  val tuba = "de.musmehl.mathezirkel" %% "tuba" % "0.0.0+116-995106bb"

}
